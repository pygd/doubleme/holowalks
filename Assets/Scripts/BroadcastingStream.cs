#if UNITY_EDITOR
#else

using System.IO;
using Windows.Foundation;
using Windows.Storage.Streams;
using UnityEngine;

namespace DoubleMe.HoloWalks
{
    public class BroadcastingStream : IRandomAccessStream
    {

        private readonly InMemoryRandomAccessStream innerStream = new InMemoryRandomAccessStream();

        public IInputStream GetInputStreamAt(ulong position)
        {
            Debug.Log($"GetInputStreamAt {position}");

            return innerStream.GetInputStreamAt(position);
        }

        public IOutputStream GetOutputStreamAt(ulong position)
        {
            Debug.Log($"GetOutputStreamAt {position}");

            return innerStream.GetOutputStreamAt(position);
        }

        public void Seek(ulong position)
        {
            Debug.Log($"Seek {position}");

            innerStream.Seek(position);
        }

        public IRandomAccessStream CloneStream()
        {
            return innerStream.CloneStream();
        }

        public bool CanRead
        {
            get
            {
                return innerStream.CanRead;
            }
        }

        public bool CanWrite
        {
            get
            {
                return innerStream.CanWrite;
            }
        }

        public ulong Position
        {
            get { return (ulong)innerStream.Position; }
        }

        public ulong Size
        {
            get { return innerStream.Size; }
            set { innerStream.Size = value; }
        }

        public IAsyncOperationWithProgress<IBuffer, uint> ReadAsync(IBuffer buffer, uint count, InputStreamOptions options)
        {
            Debug.Log($"ReadAsync {buffer.Length} {count} {options.ToString()}");

            return innerStream.ReadAsync(buffer, count, options);
        }

        public IAsyncOperationWithProgress<uint, uint> WriteAsync(IBuffer buffer)
        {
            Debug.Log($"WriteAsync {buffer.Length}");
            return innerStream.WriteAsync(buffer);
        }

        public IAsyncOperation<bool> FlushAsync()
        {
            Debug.Log($"FlushAsync");
            return innerStream.FlushAsync();
        }

        public void Dispose()
        {
            innerStream.Dispose();
        }
    }
}
#endif

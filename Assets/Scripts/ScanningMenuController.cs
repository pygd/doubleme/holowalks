﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using TMPro;
using UnityEngine;

namespace DoubleMe.HoloWalks
{
    public class ScanningMenuController : MonoBehaviour
    {
        public Animator animator;
        public TextMeshPro titleLabel;
        public SpatialMeshUploader uploader;
        public ToggleScanningVisualisation toggleScanningVisualisation;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void ResumeScanning()
        {
            //CoreServices.SpatialAwarenessSystem?.ResumeObservers();
            toggleScanningVisualisation.isScanningMeshVisible = true;
        }

        private void SuspendScanning()
        {
            //CoreServices.SpatialAwarenessSystem?.SuspendObservers();
            //CoreServices.SpatialAwarenessSystem?.ClearObservations();
            toggleScanningVisualisation.isScanningMeshVisible = false;
        }

        public void StartScanning()
        {
            animator.SetTrigger("StartScanning");
            ResumeScanning();
        }

        public void StopScanning()
        {
            animator.SetTrigger("StopScanning");
            SuspendScanning();
        }

        public void StartUploading()
        {
            SuspendScanning();
            animator.SetFloat("UploadProgress", 0);
            animator.SetTrigger("StartUploading");
            uploader.UploadMesh(
                progress=> animator.SetFloat("UploadProgress", progress),
                completed => animator.SetTrigger("UploadComplete"));
        }

        public void ChangeTitle(string newTitle)
        {
            titleLabel.text = newTitle;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace DoubleMe.HoloWalks
{
    public class SpatialMeshDownloader : MonoBehaviour
    {

        public CloudApi cloudApi;
        public Transform parent;
        public Transform playspace;
        public Material overrideMaterial;
        [SerializeField, Layer]
        public int overrideLayer;
        public float initialSize;
        public float initialDistance = 1;

        public bool IsDownloaded => parent.childCount > 0;

        // Start is called before the first frame update
        void Start()
        {
            if (cloudApi == null)
            {
                cloudApi = ScriptableObject.FindObjectOfType<CloudApi>();
            }
        }

        public void DestroyChildren()
        {
            foreach (Transform child in parent)
            {
                Destroy(child.gameObject);
            }

        }

        /// <summary>
        /// Download mesh from api and add to `parent`
        /// </summary>
        public async Task DownloadMesh(Action<float> onProgress)
        {
            // reset playspace to make sure completed gltf object get zero position and scale;
            playspace.position = Vector3.zero;
            playspace.localScale = Vector3.one;
            playspace.rotation = Quaternion.identity;

            var name = "test";

            var completed = await cloudApi.DownloadSpatialMesh(name, onProgress);
            completed.transform.parent = parent;

            repositionSpatialMesh();

            if (overrideLayer != 0)
            {
                foreach (Transform trans in completed.GetComponentsInChildren<Transform>(true))
                {
                    trans.gameObject.layer = overrideLayer;
                }
            }

            if (overrideMaterial != null)
            {
                var renderers = completed.GetComponentsInChildren<Renderer>();
                foreach (var renderer in renderers)
                {
                    renderer.material = overrideMaterial;
                }
            }
            else
            {
                var renderers = completed.GetComponentsInChildren<Renderer>();
                foreach (var renderer in renderers)
                {
                    renderer.material.DisableKeyword("_DIRECTIONAL_LIGHT");
                }
            }

            foreach (var meshFilter in completed.GetComponentsInChildren<MeshFilter>())
            {
                var collider = meshFilter.gameObject.AddComponent<MeshCollider>();
                collider.sharedMesh = meshFilter.sharedMesh;
            }
        }

        private void repositionSpatialMesh()
        {
            Bounds bounds = new Bounds();

            Renderer[] renderers = parent.GetComponentsInChildren<Renderer>();

            if (renderers.Length > 0)
            {
                //Find first enabled renderer to start encapsulate from it
                foreach (Renderer renderer in renderers)
                {
                    if (renderer.enabled)
                    {
                        bounds = renderer.bounds;
                        break;
                    }
                }
                //Encapsulate for all renderers
                foreach (Renderer renderer in renderers)
                {
                    if (renderer.enabled)
                    {
                        bounds.Encapsulate(renderer.bounds);
                    }
                }
            }

            // fit to initialSize
            if (initialSize > 0)
            {
                var desiredScale = initialSize / bounds.size.magnitude;
                playspace.localScale = new Vector3(desiredScale, desiredScale, desiredScale);
            }

            // position front of camera
            var desiredPosition = Camera.main.transform.position + Camera.main.transform.forward * initialDistance;
            var desiredOffset = desiredPosition - parent.TransformPoint(bounds.center);
            playspace.position += desiredOffset;
        }
    }
}
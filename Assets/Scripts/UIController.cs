﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoubleMe.HoloWalks {
    public class UIController : MonoBehaviour
    {
        public GameObject decoratingMenu;
        public VisitingMenuController visitingMenuController;
        public Animator decorationDrawerAnimator;
        public GameObject playspace;
        public GameObject remoteScene;

        public enum State
        {
            Idle,
            Visiting,
            Decorating,
        }

        private State _previousState = State.Idle;
        private State _currentState = State.Idle;
        public State CurrentState
        {
            get => _currentState;
            set
            {
                if (_previousState != value)
                {
                    _previousState = _currentState;
                    _currentState = value;
                    updateState();
                }
            }
        }

        void updateState()
        {
            switch (CurrentState)
            {
                case State.Idle:
                    decoratingMenu.SetActive(false);
                    visitingMenuController.gameObject.SetActive(true);
                    break;
                case State.Visiting:
                    decoratingMenu.SetActive(true);
                    visitingMenuController.gameObject.SetActive(true);
                    playspace.SetActive(true);
                    break;
                case State.Decorating:
                    decoratingMenu.SetActive(false);
                    visitingMenuController.gameObject.SetActive(false);
                    playspace.SetActive(false);
                    break;
            }
        }

        State findCurrentState()
        {
            if (decorationDrawerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Opened"))
            {
                return State.Decorating;
            }
            if (visitingMenuController.isVisiting)
            {
                return State.Visiting;
            }
            return State.Idle;
        }

        private void Start()
        {
            CurrentState = State.Idle;
        }

        private void Update()
        {
            CurrentState = findCurrentState();
        }
    }
}
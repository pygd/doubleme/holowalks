﻿using Microsoft.MixedReality.Toolkit;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DoubleMe.HoloWalks
{
    public class VisitingMenuController : MonoBehaviour
    {
        public Animator animator;
        public TextMeshPro titleLabel;
        public SpatialMeshDownloader downloader;
        public DecorationManager decorationManager;

        private float previousDownloadProgress = 0;
        private float downloadProgress = 0;

        public bool isVisiting => downloader.IsDownloaded;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (downloadProgress != previousDownloadProgress)
            {
                animator.SetFloat("DownloadProgress", downloadProgress);
                previousDownloadProgress = downloadProgress;
            }
        }

        public void OnClick()
        {
            if (downloader.IsDownloaded)
            {
                StopVisiting();
            } else
            {
                StartDownloading();
            }
        }

        public void StopVisiting()
        {
            animator.SetTrigger("StopVisiting");
            downloader.DestroyChildren();
            decorationManager.ClearDecorations();
            CoreServices.SpatialAwarenessSystem?.SpatialAwarenessObjectParent.SetActive(true);
        }

        public async void StartDownloading()
        {
            animator.SetFloat("DownloadProgress", 0);
            animator.SetTrigger("StartDownloading");
            await downloader.DownloadMesh(progress => downloadProgress = progress);
            animator.SetTrigger("DownloadComplete");
            CoreServices.SpatialAwarenessSystem?.SpatialAwarenessObjectParent.SetActive(false);
            decorationManager.LoadRemoteDecorations();
        }

        public void ChangeTitle(string newTitle)
        {
            titleLabel.text = newTitle;
        }
    }
}
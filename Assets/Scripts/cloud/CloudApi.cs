using DoubleMe.HoloWalks.models;
using GLTF;
using GLTF.Schema;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityGLTF;

namespace DoubleMe.HoloWalks
{

    public class GraphQLContent : StringContent
    {
        public GraphQLContent(string query, object variables = null) : base(JObject.FromObject(new
        {
            query = query,
            variables = variables
        }).ToString(), System.Text.Encoding.UTF8, "application/graphql")
        {
        }
    }

    [CreateAssetMenu(fileName = "cloudApi", menuName = "HoloCloud/CloudApi", order = 1)]
    public class CloudApi : ScriptableObject
    {
        string RetrieveTexturePath(Texture texture = null)
        {
            return "";//AssetDatabase.GetAssetPath(texture);
        }

        public string endpoint = "https://7mg7nblfwzaqdgr5nycgglbpfi.appsync-api.ap-northeast-2.amazonaws.com/graphql/";
        public string apiKey = "da2-6imajf7webdgvgu3snwf6odhji";

        private GraphQLMessageHandler graphQLMessageHandler => new GraphQLMessageHandler(apiKey, new HttpClientHandler());

        public CloudStorage cloudStorage;
        protected ImporterFactory importerFactory;
        protected ImporterFactory ImporterFactory
        {
            get
            {
                importerFactory = importerFactory ?? ScriptableObject.CreateInstance<DefaultImporterFactory>();
                return importerFactory;
            }
        }

        class GraphQLMessageHandler : DelegatingHandler
        {
            private string apiKey;

            public GraphQLMessageHandler(string apiKey, HttpMessageHandler innerHandler) : base(innerHandler)
            {
                this.apiKey = apiKey;
            }

            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                request.Headers.Add("x-api-key", apiKey);

                var requestContent = request.Content != null ? await request.Content.ReadAsStringAsync() : null;
                Debug.LogFormat("Request:\n{0}\n{1}", request.ToString(), requestContent);

                HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
                var responseContent = response.Content != null ? await response.Content?.ReadAsStringAsync() : null;
                Debug.LogFormat("Response:\n{0}\n{1}", response.ToString(), responseContent);

                return response;
            }
        }


        void OnEnable()
        {
            TestGraphQL();
            Debug.LogFormat("Cloud Api enabled with temp path {0}", Application.persistentDataPath);
        }

        private async void TestGraphQL()
        {
            /*
            using (var graphQLClient = new GraphQLHttpClient(graphQLHttpClientOptions))
            {
                
#pragma warning disable 618
                graphQLClient.WebSocketReceiveErrors.Subscribe(e =>
                {
                    if (e is WebSocketException we)
                        Console.WriteLine($"WebSocketException: {we.Message} (WebSocketError {we.WebSocketErrorCode}, ErrorCode {we.ErrorCode}, NativeErrorCode {we.NativeErrorCode}");
                    else
                        Console.WriteLine($"Exception in websocket receive stream: {e.ToString()}");
                });


                var listResult = await graphQLClient.SendQueryAsync("query jj{listDecorations{items{id description}}}").ConfigureAwait(false);
                Debug.Log(listResult.Data);
                var stream = graphQLClient.CreateSubscriptionStream(new GraphQLRequest(@"
                     subscription OnCreateDecoration {
                    onCreateDecoration {
                        id
                        }
                }"));
#pragma warning restore 618

                stream.Subscribe(
                response => Debug.Log($" new content from \"{response.Data}\""),
                exception => Debug.Log($" content subscription stream failed: {exception}"),
                () => Debug.Log($" content subscription stream completed"));
            }
            */
        }

        public async Task<Decoration> SaveDecoration(Decoration decoration)
        {
            using (var client = new HttpClient(graphQLMessageHandler))
            {
                var request = new GraphQLContent(@"
                    mutation CreateDecoration($input: CreateDecorationInput!) {
                        createDecoration(input: $input) {
                            id
                            description
                            transform { 
                                position {x y z}
                                rotation {x y z w}
                                scale {x y z}
                            }
                            type
                        }
                    }",
                new
                {
                    input = decoration
                });
                var response = await client.PostAsync(endpoint, request);
                var json = JObject.Parse(await response.Content.ReadAsStringAsync());
                var errors = json["errors"];
                if (errors != null)
                {
                    throw new Exception($"GraphQL Exception, {errors}");
                }
                return json["data"]["createDecoration"].ToObject<Decoration>();
            }
        }

        public async Task<Decoration[]> ListDecorations()
        {
            using (var client = new HttpClient(graphQLMessageHandler))
            {
                var request = new GraphQLContent(@"query ListDecorations {
                        listDecorations {
                            items {
                                id
                                description
                                transform { 
                                    position {x y z}
                                    rotation {x y z w}
                                    scale {x y z}
                                }
                                type
                            }
                        }
                    }");
                var response = await client.PostAsync(endpoint, request);
                var json = JObject.Parse(await response.Content.ReadAsStringAsync());
                var errors = json["errors"];
                if (errors != null)
                {
                    throw new Exception($"GraphQL Exception, {errors}");
                }
                return json["data"]["listDecorations"]["items"].ToObject<Decoration[]>();
            }
        }

        public async Task<Decoration> UpdateDecoration(Decoration decoration)
        {
            using (var client = new HttpClient(graphQLMessageHandler))
            {
                var request = new GraphQLContent(@"
                    mutation UpdateDecoration($input: UpdateDecorationInput!) {
                        updateDecoration(input: $input) {
                            id
                            description
                            transform { 
                                position {x y z}
                                rotation {x y z w}
                                scale {x y z}
                            }
                            type
                        }
                    }",
                new
                {
                    input = decoration
                });
                var response = await client.PostAsync(endpoint, request);
                var json = JObject.Parse(await response.Content.ReadAsStringAsync());
                var errors = json["errors"];
                if (errors != null)
                {
                    throw new Exception($"GraphQL Exception, {errors}");
                }
                return json["data"]["updateDecoration"].ToObject<Decoration>();
            }
        }

        public async Task<bool> DeleteDecoration(string id)
        {
            using (var client = new HttpClient(graphQLMessageHandler))
            {
                var request = new GraphQLContent(@"
                    mutation DeleteDecoration($input: DeleteDecorationInput!) {
                      deleteDecoration(input: $input) {
                        id
                      }
                    }",
                new
                {
                    input = new
                    {
                        id = id
                    }
                });
                var response = await client.PostAsync(endpoint, request);
                var json = JObject.Parse(await response.Content.ReadAsStringAsync());
                var errors = json["errors"];
                if (errors != null)
                {
                    throw new Exception($"GraphQL Exception, {errors}");
                }
                var deletedId = json["data"]["deleteDecoration"]["id"].Value<string>();
                return deletedId == id;
            }
        }

        public async Task SaveWorldAnchor(string name, byte[] data)
        {
            Debug.Log($"Saving WorldAnchor {name} with {data.Length}");
            return;
        }

        public async Task<byte[]> GetWorldAnchor(string name)
        {
            Debug.Log($"Getting WorldAnchor {name}");
            return new byte[] { };
        }

        #region Spatial Mesh Storage

        public async Task UploadSpatialMesh(string name, UnityEngine.Transform[] rootTransforms, Action<float> onProgress = null, Action<string> onComplete = null, Action<Exception> onFailed = null)
        {
            var fileName = name + ".glb";
            var filePath = Path.Combine(Application.persistentDataPath, fileName);
            var exportOptions = new ExportOptions { ExportInactivePrimitives = false };
            var exporter = new GLTFSceneExporter(rootTransforms, exportOptions);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            Debug.Log(String.Format("Saved temp glTF to {0}", filePath));
            using (var stream = new MemoryStream())
            {
                exporter.SaveGLBToStream(stream, name);
                stream.Position = 0;
                await cloudStorage.Upload(fileName, stream, onProgress);
            }
        }

        public async Task<GameObject> DownloadSpatialMesh(string name, Action<float> onProgress = null)
        {
            Debug.Log($"Download SpatialMesh name: {name}");

            var fileName = name + ".glb";

            using (var webStream = new ProgressStream(await cloudStorage.Download(fileName)))
            using (var stream = new MemoryStream())
            {
                Debug.Log($"Downloaded SpatialMesh size:{webStream.Length}");

                if (onProgress != null)
                {
                    webStream.BytesWritten += (object sender, ProgressStreamReportEventArgs args) =>
                        onProgress((float)args.StreamPosition / (float)args.StreamLength);
                }
                Debug.Log($"Cloning SpatialMesh data");
                await webStream.CopyToAsync(stream);
                var importOptions = new ImportOptions
                {
                    ThrowOnLowMemory = true
                };
                Debug.Log($"Parsing SpatialMesh glb");
                GLTFParser.ParseJson(stream, out GLTFRoot gLTFRoot);
                stream.Position = 0;
                Debug.Log($"Importing SpatialMesh glb");
                using (var importer = new GLTFSceneImporter(gLTFRoot, stream, importOptions)
                {
                    CustomShaderName = "Mixed Reality Toolkit/Standard"
                })
                {
                    Debug.Log($"Loading SpatialMesh to scene");
                    await importer.LoadSceneAsync();
                    Debug.Log($"SpatialMesh loaded successfully");
                    return importer.LastLoadedScene;
                }
            }
        }
    }
    #endregion
}
﻿// https://github.com/firebase/quickstart-unity/blob/master/storage/testapp/Assets/Firebase/Sample/Storage/UIHandler.cs

namespace DoubleMe.HoloWalks
{
    using System.IO;
    using System.Threading.Tasks;
    using System;
    using Amazon.CognitoIdentity;
    using Amazon.Runtime;
    using Amazon.S3.Model;
    using Amazon.S3;
    using Amazon;
    using UnityEditor;
    using UnityEngine;

    [CreateAssetMenu(fileName = "cloudStorage", menuName = "HoloCloud/CloudStorage", order = 1)]
    public class CloudStorage : ScriptableObject
    {
        #region AWS
        public string S3BucketName = null;
        public string IdentityPoolId = "";
        public string CognitoIdentityRegion = RegionEndpoint.APNortheast2.SystemName;
        private RegionEndpoint _CognitoIdentityRegion
        {
            get { return RegionEndpoint.GetBySystemName(CognitoIdentityRegion); }
        }
        public string S3Region = RegionEndpoint.APNortheast2.SystemName;
        private RegionEndpoint _S3Region
        {
            get { return RegionEndpoint.GetBySystemName(S3Region); }
        }

        private IAmazonS3 _s3Client;
        private AWSCredentials _credentials;

        private AWSCredentials Credentials
        {
            get
            {
                if (_credentials == null)
                    _credentials = new CognitoAWSCredentials(IdentityPoolId, _CognitoIdentityRegion);
                return _credentials;
            }
        }

        private IAmazonS3 Client
        {
            get
            {
                if (_s3Client == null)
                {
                    _s3Client = new AmazonS3Client(Credentials, _S3Region);
                }
                return _s3Client;
            }
        }

        #endregion

#if UNITY_EDITOR
        protected void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayStateChange;
        }

        protected void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayStateChange;
        }

        void OnPlayStateChange(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.EnteredPlayMode)
            {
                OnBegin();
            }
            else if (state == PlayModeStateChange.ExitingPlayMode)
            {
                OnEnd();
            }
        }
#else
        protected void OnEnable () {
            OnBegin ();
        }

        protected void OnDisable () {
            OnEnd ();
        }
#endif
        protected void OnEnd() { }

        protected void OnBegin()
        {
            var gameObject = new GameObject()
            {
                name = "AWS"
            };
            UnityInitializer.AttachToGameObject(gameObject);
            AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
        }

        public Task Upload(string name, Stream stream, Action<float> onProgress = null)
        {
            var tcs = new TaskCompletionSource<bool>();

            try
            {

                var request = new PostObjectRequest()
                {
                    Bucket = S3BucketName,
                    Key = name,
                    InputStream = stream,
                    CannedACL = S3CannedACL.PublicRead
                };

                request.Headers.ContentType = "model/gltf-binary";

                if (onProgress != null)
                {
                    request.StreamTransferProgress += new EventHandler<StreamTransferProgressArgs>((object sender, StreamTransferProgressArgs e) =>
                    {
                        onProgress((float)e.TransferredBytes / (float)e.TotalBytes);
                    });
                }

                Client.PostObjectAsync(request, (responseObj) =>
                {
                    if (responseObj.Exception == null)
                    {
                        tcs.SetException(responseObj.Exception);
                    }
                    else
                    {
                        tcs.SetResult(true);
                    }
                });
            }
            catch (Exception e)
            {
                tcs.SetException(e);
            }

            return tcs.Task;
        }

        public Task<Stream> Download(string name)
        {
            var tcs = new TaskCompletionSource<Stream>();

            try
            {
                var request = new GetObjectRequest()
                {
                    BucketName = S3BucketName,
                    Key = name
                };

                Client.GetObjectAsync(request, (responseObj) =>
                {
                    var response = responseObj.Response;

                    if (responseObj.Exception != null)
                    {
                        tcs.SetException(responseObj.Exception);
                    }
                    else if (response.ResponseStream != null)
                    {
                        tcs.SetResult(response.ResponseStream);
                    }
                    else
                    {
                        tcs.SetException(new Exception("Stream not available"));
                    }
                });
            }
            catch (Exception e)
            {
                tcs.SetException(e);
            }

            return tcs.Task;
        }
    }

}
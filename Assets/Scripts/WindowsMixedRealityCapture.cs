﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using System.Threading.Tasks;
#if UNITY_EDITOR
#else
using Windows.Media.Capture;
using Windows.Media.Capture.Frames;
using Windows.Media.Effects;
using Windows.Media.MediaProperties;
using Windows.Graphics.Imaging;
#endif

namespace DoubleMe.HoloWalks
{
    /// <summary>
    /// 
    /// </summary>
    /// https://github.com/qian256/HoloLensARToolKit/blob/master/ARToolKitUWP-Unity/Scripts/ARUWPVideo.cs
    public class WindowsMixedRealityCapture : MonoBehaviour
    {
        /// <summary>
        /// Unity Material to hold the Unity Texture of camera preview image. [internal use]
        /// </summary>
        public Material mediaMaterial = null;

        /// <summary>
        /// Unity Texture to hold the camera preview image. [internal use]
        /// </summary>
        private Texture2D mediaTexture = null;

        /// <summary>
        /// Initial value for video preview visualization. Video preview visualization might pose
        /// additional burden to rendering, but is useful for debugging tracking behavior.
        /// At runtime, please use EnablePreview() and DisablePreview() to control it. [public use]
        /// [initialization only]
        /// </summary>
        public bool videoPreview = true;

        /// <summary>
        /// Signal indicating that the video has updated between the last render loop, the previous
        /// Update() and the current Update(). [internal use]
        /// </summary>
        private bool isBitmapUpdated = false;

        /// <summary>
        /// Signal indicating that the initialization of video loop is done. [internal use]
        /// </summary>
        private bool signalInitDone = false;

#if UNITY_EDITOR
#else
        protected LowLagMediaRecording _mediaRecording;
        protected MediaCapture mediaCapture;
        protected MediaFrameReader frameReader;
        protected BroadcastingStream mediaStream;

        [ComImport]
        [Guid("5B0D3235-4DBA-4D44-865E-8F1D0E4FD04D")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        unsafe interface IMemoryBufferByteAccess
        {
            /// <summary>
            /// Unsafe function to retrieve the pointer and size information of the underlying
            /// buffer object. Must be used within unsafe functions. In addition, the project needs
            /// to be configured as "Allow unsafe code". [internal use]
            /// </summary>
            /// <param name="buffer">byte pointer to the start of the buffer</param>
            /// <param name="capacity">the size of the buffer</param>
            void GetBuffer(out byte* buffer, out uint capacity);
        }

        /// <summary>
        /// Temporary SoftwareBitmap used for data exchange between Unity UI thread (Update 
        /// function) and the video thread (OnFrameArrived function). [internal use]
        /// </summary>
        private SoftwareBitmap _bitmap = null;

        /// <summary>
        /// The SoftwareBitmap used for Unity UI thread for video previewing. [internal use]
        /// </summary>
        private SoftwareBitmap updateBitmap = null;

        /// <summary>
        /// Size of the video buffer used to create Unity texture object. [internal use]
        /// </summary>
        private int videoBufferSize = 0;
        private int frameHeight;
        private int frameWidth;

        // private BroadcastingSink broadcastingSink;

        // Create and initialze the MediaCapture object.
        protected async Task<bool> InitMediaCapture()
        {
            Debug.Log("Initializing MediaCapture");
            mediaCapture = new MediaCapture();

            mediaCapture.Failed += (MediaCapture sender, MediaCaptureFailedEventArgs errorEventArgs) => Debug.LogWarning(errorEventArgs);

            var allGroups = await MediaFrameSourceGroup.FindAllAsync();
            foreach (var group in allGroups)
            {
                Debug.Log(group.DisplayName + ", " + group.Id);
            }

            if (allGroups.Count <= 0)
            {
                Debug.Log("InitializeMediaCaptureAsyncTask() fails because there is no MediaFrameSourceGroup");
                return false;
            }

            var settings = new MediaCaptureInitializationSettings
            {
                SourceGroup = allGroups[0],
                // This media capture can share streaming with other apps.
                SharingMode = MediaCaptureSharingMode.SharedReadOnly,
                // Only stream video and don't initialize audio capture devices.
                StreamingCaptureMode = StreamingCaptureMode.AudioAndVideo,
                // Set to CPU to ensure frames always contain CPU SoftwareBitmap images
                // instead of preferring GPU D3DSurface images.
                MemoryPreference = MediaCaptureMemoryPreference.Cpu
            };
            await mediaCapture.InitializeAsync(settings);
            await mediaCapture.AddVideoEffectAsync(new VideoEffectDefinition("Windows.Media.MixedRealityCapture.MixedRealityCaptureVideoEffect"), MediaStreamType.VideoRecord);

            Debug.Log("Initializing Media Preview");
            var mediaFrameSourceVideoPreview = mediaCapture.FrameSources.Values.Single(x => x.Info.MediaStreamType == MediaStreamType.VideoPreview);
            frameReader = await mediaCapture.CreateFrameReaderAsync(mediaFrameSourceVideoPreview);
            frameReader.FrameArrived += OnFrameArrived;
            frameWidth = Convert.ToInt32(mediaFrameSourceVideoPreview.CurrentFormat.VideoFormat.Width);
            frameHeight = Convert.ToInt32(mediaFrameSourceVideoPreview.CurrentFormat.VideoFormat.Height);
            videoBufferSize = frameWidth * frameHeight * 4;
            MediaFrameReaderStartStatus mediaFrameReaderStartStatus = await frameReader.StartAsync();
            Debug.LogFormat("FrameReader is {2} initialized, {0}x{1}", frameWidth, frameHeight, mediaFrameReaderStartStatus);

            signalInitDone = true;
            return true;
        }

        /// <summary>
        /// The callback that is triggered when new video preview frame arrives. In this function,
        /// video frame is saved for Unity UI if videoPreview is enabled, tracking task is triggered
        /// in this function call, and video FPS is recorded. [internal use]
        /// </summary>
        /// https://github.com/qian256/HoloLensARToolKit/blob/master/ARToolKitUWP-Unity/Scripts/ARUWPVideo.cs
        /// <param name="sender">MediaFrameReader object</param>
        /// <param name="args">arguments not used here</param>
        protected void OnFrameArrived(MediaFrameReader sender, MediaFrameArrivedEventArgs args)
        {
            if (videoPreview)
            {
                var mediaFrameReference = sender.TryAcquireLatestFrame();
                var videoMediaFrame = mediaFrameReference?.VideoMediaFrame;
                var softwareBitmap = videoMediaFrame?.SoftwareBitmap;

                if (softwareBitmap != null)
                {
                    if (softwareBitmap.BitmapPixelFormat != Windows.Graphics.Imaging.BitmapPixelFormat.Bgra8 ||
                        softwareBitmap.BitmapAlphaMode != Windows.Graphics.Imaging.BitmapAlphaMode.Premultiplied)
                    {
                        softwareBitmap = SoftwareBitmap.Convert(softwareBitmap, BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied);
                    }

                    // Swap the processed frame to _backBuffer and dispose of the unused image.
                    softwareBitmap = Interlocked.Exchange(ref _bitmap, softwareBitmap);
                    isBitmapUpdated = true;
                    softwareBitmap?.Dispose();
                }

                mediaFrameReference.Dispose();
            }
        }

        /// <summary>
        /// Update the video preview texture. It is an unsafe function since unsafe interface is
        /// used. Here the memory exchange between video thread and Unity UI thread happens, and
        /// refresh Unity Texture object based on video SoftwareBitmap. [internal use]
        /// </summary>
        private unsafe void UpdateVideoPreview()
        {
            Interlocked.Exchange(ref updateBitmap, _bitmap);
            using (var input = updateBitmap.LockBuffer(BitmapBufferAccessMode.Read))
            {
                using (var inputReference = input.CreateReference())
                {
                    byte* inputBytes;
                    uint inputCapacity;
                    ((IMemoryBufferByteAccess)inputReference).GetBuffer(out inputBytes, out inputCapacity);
                    mediaTexture.LoadRawTextureData((IntPtr)inputBytes, videoBufferSize);
                    mediaTexture.Apply();
                }
            }
        }

        protected async Task startCapture()
        {
            mediaStream = new BroadcastingStream();
            //mediaStream.BytesRead += (object sender, ProgressStreamReportEventArgs args) => Debug.Log($"BytesRead {args.BytesMoved} {args.StreamLength}/{args.StreamPosition}");
            //mediaStream.BytesWritten += (object sender, ProgressStreamReportEventArgs args) => Debug.Log($"BytesWritten {args.BytesMoved} {args.StreamLength}/{args.StreamPosition}");
            //mediaStream.BytesMoved += (object sender, ProgressStreamReportEventArgs args) => Debug.Log($"BytesMoved {args.BytesMoved} {args.StreamLength}/{args.StreamPosition}");
            Debug.Log("Start Capturing");
            _mediaRecording = await mediaCapture.PrepareLowLagRecordToStreamAsync(MediaEncodingProfile.CreateHevc(VideoEncodingQuality.Auto), mediaStream);
            await _mediaRecording.StartAsync();
            Debug.Log("Started Capturing");

            //await mediaCapture.StartRecordToCustomSinkAsync(MediaEncodingProfile.CreateHevc(VideoEncodingQuality.Auto), broadcastingSink);
            //await captureManager.StartRecordToStreamAsync(MediaEncodingProfile.CreateHevc(VideoEncodingQuality.Auto), mediaStream.AsRandomAccessStream());
        }

        async void Start()
        {
            await InitMediaCapture();
            // await startCapture();
        }
        void Update()
        {
            if (signalInitDone)
            {
                if (mediaMaterial != null)
                {
                    mediaTexture = new Texture2D(frameWidth, frameHeight, TextureFormat.RGBA32, false);
                    mediaMaterial.mainTexture = mediaTexture;
                }
                signalInitDone = false;
            }

            if (isBitmapUpdated)
            {
                if (videoPreview && mediaMaterial != null)
                {
                    UpdateVideoPreview();
                }
                isBitmapUpdated = false;
            }

        }
#endif
    }
}
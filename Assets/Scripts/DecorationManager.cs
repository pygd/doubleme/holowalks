﻿using DoubleMe.HoloWalks.models;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Experimental.Utilities;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Transform = UnityEngine.Transform;
using Vector3 = UnityEngine.Vector3;
using Quaternion = UnityEngine.Quaternion;

#if false // UNITY_WSA
using UnityEngine.XR.WSA.Sharing;
using UnityEngine.XR.WSA;
#endif

namespace DoubleMe.HoloWalks
{
    public enum DecorationType
    {
        Default,
        Image,
        Video,
        Spatial
    }
    public class DecorationManager : MonoBehaviour
    {
        public CloudApi cloudApi;
        public WorldAnchorManager worldAnchorManager;
        public GameObject BaseDecorationPrefeb;
        public GameObject ImageDecorationPrefab;
        public GameObject VideoDecorationPrefab;
        public GameObject SpatialDecoraionPrefab;
        private UnityEngine.Transform DecorationContainer => transform;

        public class DecorationPointerPlacementHandler : IMixedRealityPointerHandler
        {
            public Action OnPlaced;

            private Transform TargetTransform;
            private Transform FinalTransform;

            public DecorationPointerPlacementHandler(Transform targetTransform, Transform finalTransform)
            {
                TargetTransform = targetTransform ?? throw new ArgumentNullException(nameof(targetTransform));
                FinalTransform = finalTransform;

                CoreServices.InputSystem?.RegisterHandler<IMixedRealityPointerHandler>(this);
            }

            public void OnPointerClicked(MixedRealityPointerEventData eventData)
            {
                TargetTransform.parent = FinalTransform;
                CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
                OnPlaced?.Invoke();
            }

            public void OnPointerDown(MixedRealityPointerEventData eventData)
            {
            }

            public void OnPointerDragged(MixedRealityPointerEventData eventData)
            {

            }

            public void OnPointerUp(MixedRealityPointerEventData eventData)
            {
            }
        }

#if false // UNITY_WSA
        public bool isAnchored => gameObject.GetComponent<WorldAnchor>() != null;

        private WorldAnchorStore store;
        private WorldAnchor gameRootAnchor;

        private void StoreLoaded(WorldAnchorStore store)
        {
            Debug.Log($"WorldAnchorStore loaded with {store.anchorCount} anchors");
            this.store = store;
            SetWorldAnchor();
        }

        private async Task<bool> LoadWorldAnchor(int retry = 5)
        {
            Debug.Log($"LoadWorldAnchor");
            var data = await cloudApi.GetWorldAnchor("decoration");

            var tcs = new TaskCompletionSource<bool>();

            try
            {
                int retryCount = retry;

                WorldAnchorTransferBatch.DeserializationCompleteDelegate importCB = null;

                importCB = (completionReason, deserializedTransferBatch) =>
                {
                    if (completionReason != SerializationCompletionReason.Succeeded)
                    {
                        Debug.Log($"{retryCount} Failed to import: " + completionReason.ToString());
                        if (retryCount > 0)
                        {
                            retryCount--;
                            WorldAnchorTransferBatch.ImportAsync(data, importCB);
                        }
                        else
                        {
                            tcs.SetResult(false);
                        }
                        return;
                    }
                    else
                    {
                        tcs.SetResult(true);
                    }
                    gameRootAnchor = deserializedTransferBatch.LockObject("decoration", gameObject);
                };

                WorldAnchorTransferBatch.ImportAsync(data, importCB);
            }
            catch (Exception e)
            {
                tcs.SetException(e);
            }

            return await tcs.Task;
        }

        private async void SetWorldAnchor()
        {
            var loaded = await LoadWorldAnchor();

            if (gameRootAnchor == null)
            {
                gameRootAnchor = gameObject.AddComponent<WorldAnchor>();
                this.store.Save("decoration", gameRootAnchor);
                ExportGameRootAnchor();
            }
        }
        private void ExportGameRootAnchor()
        {
            Debug.Log("Exporting WorldAnchor");
            WorldAnchorTransferBatch transferBatch = new WorldAnchorTransferBatch();
            transferBatch.AddWorldAnchor("decoration", this.gameRootAnchor);
            WorldAnchorTransferBatch.ExportAsync(transferBatch, OnExportDataAvailable, OnExportComplete);
        }

        private async void OnExportDataAvailable(byte[] data)
        {
            await cloudApi.SaveWorldAnchor("decoration", data);
        }

        private void OnExportComplete(SerializationCompletionReason completionReason)
        {
            if (completionReason != SerializationCompletionReason.Succeeded)
            {
                Debug.LogError("Decoration world anchor export failed");
            }
            else
            {
                Debug.LogError("Decoration world anchor saved");
            }
        }
#endif
        private DecorationType GetRandomDecorationType()
        {
            switch (UnityEngine.Random.Range(0, 2))
            {
                default:
                    return DecorationType.Default;
                case 1:
                    return DecorationType.Video;
            }
        }

        private DecorationType GetDecorationType(string type)
        {
            switch (type.ToLower())
            {
                default:
                    return DecorationType.Default;
                case "image":
                    return DecorationType.Image;
                case "video":
                    return DecorationType.Video;
                case "spatial":
                    return DecorationType.Spatial;
            }
        }

        private GameObject GetDecorationPrefab(DecorationType type)
        {
            switch (type)
            {
                default:
                    return BaseDecorationPrefeb;
                case DecorationType.Image:
                    return ImageDecorationPrefab;
                case DecorationType.Video:
                    return VideoDecorationPrefab;
                case DecorationType.Spatial:
                    return SpatialDecoraionPrefab;
            }
        }

        private void Start()
        {
            Debug.Log(DecorationType.Video.ToString());

#if UNITY_WSA
            // WorldAnchorStore.GetAsync(StoreLoaded);
#endif

        }

        private void Update()
        {
        }

        public void CreatePinDecoration()
        {
            CreateDecoration(BaseDecorationPrefeb);
        }

        public async void CreateImageDecoration(string path)
        {
            var decoration = CreateDecoration(ImageDecorationPrefab);
            var imageDecoration = decoration.GetComponent<ImageDecoration>();
            await imageDecoration.SetImagePath(path);
        }

        public void CreateVideoDecoration(string path)
        {
            var decoration = CreateDecoration(VideoDecorationPrefab);
            var video = decoration.GetComponent<VideoDecoration>();
            video.SetVideoPath(path);
        }

        public async void CreateSpatialDecoration(string path)
        {
            var decoration = CreateDecoration(SpatialDecoraionPrefab);
            var spatial = decoration.GetComponent<SpatialDecoration>();
            await spatial.SetGlbPath(path);
        }

        private GameObject CreateDecoration(GameObject decorationPrefab)
        {
            var cursorTransform = CoreServices.InputSystem.FocusProvider.PrimaryPointer.BaseCursor?.GameObjectReference?.transform;
            if (cursorTransform == null)
            {
                cursorTransform = GameObject.Find("DefaultCursor(Clone)").transform;
                Debug.LogWarning($"Forcing to DefaultCursor transform: {cursorTransform}");
            }
            Debug.Log($"Creating decoration to {cursorTransform}");
            var newDecoration = Instantiate(decorationPrefab, DecorationContainer);

            var decorationController = newDecoration.GetComponent<DecorationController>();
            decorationController.Displace(cursorTransform);

            new DecorationPointerPlacementHandler(decorationController.HostTransform, newDecoration.transform)
            {
                OnPlaced = () => decorationController.Place(true)
            };

            return newDecoration;
        }

        [Obsolete]
        public void CreateDecoration(UnityEngine.Vector3 position, UnityEngine.Quaternion rotation)
        {
            var type = GetRandomDecorationType();
            var newDecoration = Instantiate(GetDecorationPrefab(type), position, rotation, DecorationContainer);
            // SaveDecoration(newDecoration, type);
        }

        public void LoadRemoteDecorations()
        {
            LoadDecorations();
        }

        public void ClearDecorations()
        {
            foreach (Transform child in DecorationContainer.transform)
            {
                Destroy(child.gameObject);
            }
        }

        protected async void LoadDecorations()
        {
            var loaded = await cloudApi.ListDecorations();
            var currentThread = Thread.CurrentThread;
            foreach (var decoration in loaded)
            {
                var type = GetDecorationType(decoration.type);
                var newDecoration = Instantiate(GetDecorationPrefab(type), DecorationContainer);
                var decorationController = newDecoration.GetComponent<DecorationController>();
                decorationController.Displace();
                await decorationController.SetDecoration(decoration);
                decorationController.Place();
            }
            Debug.LogFormat("Loaded {0} Decorations", loaded.Length);
        }

        protected async Task SaveDecoration(DecorationController decorationController, DecorationMetadata metadata = null)
        {
            // TODO: fix description and metadata (when server work is done)
            var saved = await cloudApi.SaveDecoration(new Decoration
            {
                description = decorationController.Description,
                transform = decorationController.HostTransform,
                type = decorationController.Type.ToString(),
                // metadata = metadata
            });
            decorationController.Decoration = saved;
            //worldAnchorManager.AttachAnchor(gameObject);
            Debug.LogFormat("Saved Decoration({0})", saved.id);

            var appBar = decorationController.GetComponentInChildren<AppBar>();
        }

        protected async Task UpdateDecoration(DecorationController decorationController)
        {
            // TODO: fix description and metadata (when server work is done)
            var saved = await cloudApi.UpdateDecoration(new Decoration
            {
                id = decorationController.Decoration.id,
                description = decorationController.Description,
                transform = decorationController.HostTransform,
                type = decorationController.Type.ToString(),
                // metadata = metadata
            });
            decorationController.Decoration = saved;
            //worldAnchorManager.AttachAnchor(gameObject);
            Debug.LogFormat("Updated Decoration({0})", saved.id);
        }

        public async Task DeleteDecoration(DecorationController decorationController)
        {
            var deleted = await cloudApi.DeleteDecoration(decorationController.Decoration.id);
            if (deleted) Destroy(decorationController.gameObject);
        }

        public async Task UpsertDecoration(DecorationController decoration)
        {
            if (String.IsNullOrWhiteSpace(decoration.Decoration?.id)) {
                await SaveDecoration(decoration);
            }
            else
            {
                await UpdateDecoration(decoration);
            }
        }
    }
}

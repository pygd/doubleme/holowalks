﻿using DoubleMe.HoloWalks.models;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

using Transform = UnityEngine.Transform;
using Vector3 = UnityEngine.Vector3;

namespace DoubleMe.HoloWalks
{
    /// <summary>
    /// Control single decoration lifecycle
    /// </summary>
    /// <remarks>Shoul be placed on root of decoration prefab</remarks>
    public class DecorationController : MonoBehaviour
    {
        private const string IgnoreLayerName = "Ignore Raycast";

        public virtual DecorationType Type => DecorationType.Default;

        public GameObject DecorationContainer;
        protected DecorationManager decorationManager => FindObjectOfType(typeof(DecorationManager)) as DecorationManager;

        public virtual Transform HostTransform => DecorationContainer.transform;

        public string Description = null;

        private Decoration _decoration;
        public virtual Decoration Decoration
        {
            get => _decoration;
            set => _ = SetDecoration(value);
        }


        protected virtual Task WillSetDecoration(Decoration value)
        {
            return Task.CompletedTask;
        }

        public virtual async Task SetDecoration(Decoration value)
        {
            await WillSetDecoration(value);

            gameObject.name = $"({value.type}){value.id}";
            HostTransform.localPosition = value.transform.position;
            HostTransform.localRotation = value.transform.rotation;
            HostTransform.localScale = value.transform.scale;
            _decoration = value;
        }

        protected AppBar appBar => GetComponentInChildren<AppBar>(true);
        protected BoundingBox boundingBox => GetComponentInChildren<BoundingBox>(true);
        protected NearInteractionGrabbable nearIGrabbable => GetComponentInChildren<NearInteractionGrabbable>();
        protected ManipulationHandler manipulationHandler => GetComponentInChildren<ManipulationHandler>();

        private Dictionary<GameObject, int> layerCache;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (appBar != null && appBar.transform.lossyScale.magnitude < 1)
            {
                var normalizedScale = Vector3.one.Div(appBar.transform.lossyScale);
                appBar.transform.localScale = normalizedScale;
            }
        }

        public void ToggleAppBar()
        {
            if (appBar != null && appBar.State != AppBar.AppBarStateEnum.Manipulation)
            {
                var appBarActive = appBar.gameObject.activeSelf;
                appBar.gameObject.SetActive(!appBarActive);
            }
        }

        private void SetEdit(bool edit = false)
        {
            if (appBar != null)
            {
                appBar.gameObject.SetActive(edit);
                appBar.State = edit ? AppBar.AppBarStateEnum.Manipulation : AppBar.AppBarStateEnum.Hidden;
            }

            if (boundingBox != null)
            {
                boundingBox.Active = edit;
            }

            if (nearIGrabbable != null) nearIGrabbable.enabled = edit;

            if (manipulationHandler != null) manipulationHandler.enabled = edit;
        }

        public virtual void Place(bool edit = false)
        {
            gameObject.ApplyLayerCacheRecursively(layerCache);

            SetEdit(edit);
        }

        public virtual void Displace(Transform parent = null)
        {
            // Ignore Raycast
            gameObject.SetLayerRecursively(LayerMask.NameToLayer(IgnoreLayerName), out layerCache);

            // Reposition
            if (parent != null)
            {
                HostTransform.position = parent.position;
                HostTransform.rotation = parent.rotation;
                HostTransform.parent = parent;
                HostTransform.localScale = Vector3.one;
            }

            SetEdit(false);
        }

        public virtual async void OnDone()
        {
            await decorationManager.UpsertDecoration(this);
        }

        public virtual async void OnRemove()
        {
            await decorationManager.DeleteDecoration(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DoubleMe.HoloWalks.models
{
    public class Decoration
    {
        public string id;
        public string owner;
        public string type;
        public string description;
        public Transform transform;
        //public DecorationMetadata metadata;
        //public string room;
    }

    public class DecorationList
    {
        public Decoration[] items;
    }

    public class DecorationMetadata {
        public string dataPath;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DoubleMe.HoloWalks.models
{
    public class Transform
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale; 

        public static implicit operator Transform(UnityEngine.Transform t) => new Transform
        {
            position = t.localPosition,
            rotation = t.localRotation,
            scale = t.localScale
        };
    }

    public class Quaternion
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public static implicit operator UnityEngine.Quaternion(Quaternion q) => new UnityEngine.Quaternion(q.x, q.y, q.z, q.w);
        public static implicit operator Quaternion(UnityEngine.Quaternion q) => new Quaternion { x = q.x, y = q.y, z = q.z, w = q.w };
    }
    public class Vector3
    {
        public float x;
        public float y;
        public float z;

        public static implicit operator UnityEngine.Vector3(Vector3 p) => new UnityEngine.Vector3(p.x, p.y, p.z);
        public static implicit operator Vector3(UnityEngine.Vector3 p) => new Vector3
        {
            x = p.x,
            y = p.y,
            z = p.z
        };
    }
}

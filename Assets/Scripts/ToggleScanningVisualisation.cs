﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using TMPro;

namespace DoubleMe.HoloWalks
{
    public class ToggleScanningVisualisation : MonoBehaviour
    {
        public TextMeshPro[] actionLabels;

        private bool _isScanningMeshVisible;
        public bool isScanningMeshVisible
        {
            get
            {
                return _isScanningMeshVisible;
            }
            set
            {
                _isScanningMeshVisible = value;
                UpdateScanningMeshVisibility();
                UpdateLabels();
            }
        }

        void UpdateLabels()
        {
            string actionText = isScanningMeshVisible ? "Hide" : "Show";
            foreach (TextMeshPro label in actionLabels)
            {
                label.text = actionText;
            }
        }

        void UpdateScanningMeshVisibility()
        {
            if (CoreServices.SpatialAwarenessSystem != null)
            {
                IMixedRealitySpatialAwarenessMeshObserver meshObserver = ((IMixedRealityDataProviderAccess)CoreServices.SpatialAwarenessSystem).GetDataProvider<IMixedRealitySpatialAwarenessMeshObserver>();
                meshObserver.DisplayOption = isScanningMeshVisible ? SpatialAwarenessMeshDisplayOptions.Visible : SpatialAwarenessMeshDisplayOptions.None;
            }
        }

        /// <summary>
        /// Initial setting of scanning mesh visualization
        /// </summary>
        void Start()
        {
            isScanningMeshVisible = false;
        }

        /// <summary>
        /// Toggles scanning mesh visualization
        /// </summary>
        public void OnToggleScanningMesh()
        {
            isScanningMeshVisible = !isScanningMeshVisible;
        }
    }

}
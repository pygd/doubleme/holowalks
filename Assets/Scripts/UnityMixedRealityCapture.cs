﻿/*
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.WebCam;

namespace DoubleMe.HoloWalks
{
    public class UnityMixedRealityCapture : MonoBehaviour
    {
        protected VideoCapture m_VideoCapture;
        protected Stream mediaStream;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        // Create and initialze the MediaCapture object.
        protected void InitMediaCapture()
        {
            VideoCapture.CreateAsync(true, OnVideoCaptureCreated);
        }

        void OnVideoCaptureCreated(VideoCapture videoCapture)
        {
            if (videoCapture != null)
            {
                m_VideoCapture = videoCapture;

                Resolution cameraResolution = VideoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
                float cameraFramerate = VideoCapture.GetSupportedFrameRatesForResolution(cameraResolution).OrderByDescending((fps) => fps).First();

                CameraParameters cameraParameters = new CameraParameters
                {
                    hologramOpacity = 0.9f,
                    frameRate = cameraFramerate,
                    cameraResolutionWidth = cameraResolution.width,
                    cameraResolutionHeight = cameraResolution.height,
                    pixelFormat = CapturePixelFormat.BGRA32
                };

                m_VideoCapture.StartVideoModeAsync(cameraParameters,
                                                    VideoCapture.AudioState.ApplicationAndMicAudio,
                                                    OnStartedVideoCaptureMode);
            }
            else
            {
                Debug.LogError("Failed to create VideoCapture Instance!");
            }
        }

        void OnStartedVideoCaptureMode(VideoCapture.VideoCaptureResult result)
        {
            if (result.success)
            {
                string filename = string.Format("MyVideo_{0}.mp4", Time.time);
                string filepath = System.IO.Path.Combine(Application.persistentDataPath, filename);

                m_VideoCapture.StartRecordingAsync(filepath, OnStartedRecordingVideo);
            }
        }

        void OnStartedRecordingVideo(VideoCapture.VideoCaptureResult result)
        {
            Debug.Log("Started Recording Video!");
            // We will stop the video from recording via other input such as a timer or a tap, etc.
        }

        void StopRecordingVideo()
        {
            m_VideoCapture.StopRecordingAsync(OnStoppedRecordingVideo);
        }

        void OnStoppedRecordingVideo(VideoCapture.VideoCaptureResult result)
        {
            Debug.Log("Stopped Recording Video!");
            m_VideoCapture.StopVideoModeAsync(OnStoppedVideoCaptureMode);
        }

        void OnStoppedVideoCaptureMode(VideoCapture.VideoCaptureResult result)
        {
            m_VideoCapture.Dispose();
            m_VideoCapture = null;
        }

        protected void startCapture()
        {
            mediaStream = new MemoryStream();
        }
    }
}
*/
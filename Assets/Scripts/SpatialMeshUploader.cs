﻿namespace DoubleMe.HoloWalks
{
    using System;
    using System.Linq;
    using UnityEngine;
    using Microsoft.MixedReality.Toolkit;

    public class SpatialMeshUploader : MonoBehaviour
    {
        public CloudApi cloudApi;

        // Start is called before the first frame update
        void Start()
        {
            if (cloudApi == null)
            {
                cloudApi = ScriptableObject.FindObjectOfType<CloudApi>();
            }
        }

        /// <summary>
        /// Optimize meshses and remove degenerate triagles (which causes problem in glTF)
        /// </summary>
        /// <param name="rooTransforms">transforms and its children to optimize</param>
        protected void Optimize(Transform[] rootTransforms)
        {
            var meshFilters = rootTransforms.SelectMany(transform => transform.GetComponentsInChildren<MeshFilter>(true));
            Debug.Log(String.Format("Optimizing {0} meshes in {1} rootTransforms", meshFilters.Count(), rootTransforms.Length));
            foreach (var meshFilter in meshFilters)
            {
                if (meshFilter.sharedMesh != null)
                {
                    meshFilter.sharedMesh.RecalculateNormals();
                    meshFilter.sharedMesh.RecalculateTangents();
                }
                else
                {
                    Debug.LogWarningFormat("Destroying empty {0}", meshFilter.name);
                    DestroyImmediate(meshFilter.gameObject);
                }

            }
        }

        public async void UploadMesh(Action<float> onProgress = null, Action<bool> onComplete = null)
        {
            Transform[] transforms =
            {
                CoreServices.SpatialAwarenessSystem?.SpatialAwarenessObjectParent.transform
            };

            Optimize(transforms);

            var name = "test"; //String.Format("{0}_spatialMesh.glb", DateTime.Now.ToString("yyMMddTHHmmss"));

            await cloudApi.UploadSpatialMesh(name, transforms);

            onComplete?.Invoke(true);

            /*
            StartCoroutine(cloudApi.UploadSpatialMesh(name, transforms.ToArray(),
                progress => {
                    Debug.Log(String.Format("Uploading {3} {0}: {1} out of {2}", progress.Reference.Name,
                                        progress.BytesTransferred, progress.TotalByteCount, (float)progress.BytesTransferred / (float)progress.TotalByteCount));
                    onProgress?.Invoke((float)progress.BytesTransferred / (float)progress.TotalByteCount);
                },
                completed =>
                {
                    Debug.Log(String.Format("Finished uploading {0}", completed.Name));
                    onComplete?.Invoke(true);
                },
                failed =>
                {
                    Debug.Log(String.Format("Error Code: {0}", failed.ErrorCode));
                    Debug.Log(String.Format("HTTP Result Code: {0}", failed.HttpResultCode));
                    Debug.Log(String.Format("Recoverable: {0}", failed.IsRecoverableException));
                    Debug.Log(failed.ToString());
                }));
                */
        }


    }

}
﻿namespace DoubleMe.HoloWalks
{
    using models;
    using Microsoft.MixedReality.Toolkit;
    using Microsoft.MixedReality.Toolkit.Input;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine;
    using Microsoft.MixedReality.Toolkit.Experimental.Utilities;
    using System.Threading.Tasks;
    using Transform = UnityEngine.Transform;
    using Quaternion = UnityEngine.Quaternion;

    public class PointerMenuController : MonoBehaviour, IMixedRealityFocusHandler, IMixedRealityPointerHandler
    {
        public enum MenuType
        {
            None,
            Self,
            Surrogate,
            Remote,
        }

        private MenuType currentMenuType;
        public DecorationManager decorationManager;

        Transform[] SurrogateMenuTargets => new Transform[] { CoreServices.SpatialAwarenessSystem?.SpatialAwarenessObjectParent.transform };
        public Transform[] RemoteMenuTargets;
        public GameObject Cursor;


        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnEnable()
        {
            // Instruct Input System that we would like to receive all input events of type 
            // IMixedRealitySourceStateHandler and IMixedRealityHandJointHandler
            CoreServices.InputSystem?.RegisterHandler<IMixedRealityFocusHandler>(this);
            CoreServices.InputSystem?.RegisterHandler<IMixedRealityPointerHandler>(this);
        }

        private void OnDisable()
        {
            // This component is being destroyed
            // Instruct the Input System to disregard us for input event handling
            CoreServices.InputSystem?.UnregisterHandler<IMixedRealityFocusHandler>(this);
            CoreServices.InputSystem?.UnregisterHandler<IMixedRealityPointerHandler>(this);
        }

        private MenuType GetMenuTypeFromObject(GameObject gameObject)
        {
            if (gameObject == null)
            {
                return MenuType.None;
            } else if (gameObject.transform.IsChildOf(Cursor.transform))
            {
                return MenuType.Self;
            } else if (SurrogateMenuTargets.Any(transform => gameObject.transform.IsChildOf(transform)))
            {
                return MenuType.Surrogate;
            } else if (RemoteMenuTargets.Any(transform => gameObject.transform.IsChildOf(transform)))
            {
                return MenuType.Remote;
            } else {
                return MenuType.None;
            }
        }

        public void OnFocusEnter(FocusEventData eventData)
        {
            var newMenuType = GetMenuTypeFromObject(eventData.NewFocusedObject);
            if (newMenuType != currentMenuType && newMenuType != MenuType.Self)
            {
                var parentTransform = eventData.Pointer.BaseCursor?.GameObjectReference?.transform;
                Cursor.transform.SetParent(parentTransform, false);
                Cursor.SetActive(parentTransform != null);
                currentMenuType = newMenuType;
                // Debug.LogFormat("OnFocusEnter {0}", newMenuType);
            }
        }

        public void OnFocusExit(FocusEventData eventData)
        {
            var oldMenuType = GetMenuTypeFromObject(eventData.OldFocusedObject);
            var newMenuType = GetMenuTypeFromObject(eventData.NewFocusedObject);
            // if (oldMenuType != newMenuType && oldMenuType == currentMenuType && newMenuType == MenuType.None)
            if (oldMenuType == currentMenuType && newMenuType == MenuType.None)
            {
                Cursor.transform.SetParent(null, false);
                Cursor.SetActive(false);
                currentMenuType = MenuType.None;
                // Debug.LogFormat("OnFocusExit {0} for {1}", oldMenuType, eventData.NewFocusedObject?.name);
            }
        }

        public void OnPointerClicked(MixedRealityPointerEventData eventData)
        {
            if (currentMenuType != MenuType.None)
            {
                var result = eventData.Pointer.Result;
                decorationManager.CreateDecoration(result.Details.Point, Quaternion.LookRotation(result.Details.Normal));
            }
        }

        public void OnPointerDown(MixedRealityPointerEventData eventData)
        {
            // Debug.Log("OnPointerDown");
        }

        public void OnPointerDragged(MixedRealityPointerEventData eventData)
        {
            // Debug.Log("OnPointerDragged");
        }

        public void OnPointerUp(MixedRealityPointerEventData eventData)
        {
            // Debug.Log("OnPointerUp");
        }
    }
}
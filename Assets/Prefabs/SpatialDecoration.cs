﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DoubleMe.HoloWalks.models;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;
using UnityGLTF;
using Microsoft.MixedReality.Toolkit;
using Transform = UnityEngine.Transform;
using Vector3 = UnityEngine.Vector3;

namespace DoubleMe.HoloWalks
{
    public class SpatialDecoration : DecorationController
    {
        public override DecorationType Type => DecorationType.Spatial;

        public GameObject loader;

        private GLTFComponent gltfComponent => DecorationContainer.GetComponentInChildren<GLTFComponent>(true);

        override protected async Task WillSetDecoration(Decoration value)
        {
            await SetGlbPath(value.description);
            await base.WillSetDecoration(value);
        }

        private bool isLoaded;
        public bool IsLoaded {
            get => isLoaded;
            private set
            {
                isLoaded = value;
                gltfComponent?.gameObject.SetActive(isLoaded);
                loader?.SetActive(!isLoaded);
            }
        }

        public override void Place(bool edit)
        {
            base.Place(edit);
            gltfComponent.LastLoadedScene.SetLayerRecursively(gameObject.layer);
        }        

        public async Task SetGlbPath(String path)
        {
            IsLoaded = false;
            Description = path;
            gltfComponent.GLTFUri = path;
            await gltfComponent.Load();
            gltfComponent.LastLoadedScene.SetLayerRecursively(gameObject.layer);
            IsLoaded = true;
            boundingBox.CreateRig();
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DoubleMe.HoloWalks.models;
using UnityEngine;
using UnityEngine.Networking;

using Transform = UnityEngine.Transform;
using Vector3 = UnityEngine.Vector3;

namespace DoubleMe.HoloWalks
{
    public class ImageDecoration : DecorationController
    {
        public override DecorationType Type => DecorationType.Image;

        override protected async Task WillSetDecoration(Decoration value)
        {
            await SetImagePath(value.description);
            await base.WillSetDecoration(value);
        }
        
        public async Task SetImagePath(String path)
        {
            Description = path;
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(path);
            await www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogWarning(www.error);
            }
            else
            {
                var scale = 0.1f;
                var imageRenderer = DecorationContainer.GetComponent<Renderer>();
                var texture = DownloadHandlerTexture.GetContent(www);
                DecorationContainer.transform.localScale = new Vector3((float)texture.width / (float)texture.height * scale, 1 * scale, 1 * scale);
                imageRenderer.material.mainTexture = texture;
            }

        }
    }
}
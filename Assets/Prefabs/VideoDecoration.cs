﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DoubleMe.HoloWalks.models;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;
using Transform = UnityEngine.Transform;
using Vector3 = UnityEngine.Vector3;

namespace DoubleMe.HoloWalks
{
    public class VideoDecoration : DecorationController
    {
        public override DecorationType Type => DecorationType.Video;

        override protected async Task WillSetDecoration(Decoration value)
        {
            SetVideoPath(value.description);
            await base.WillSetDecoration(value);
        }

        public void SetVideoPath(String path)
        {
            Description = path;

            var videoPlayer = DecorationContainer.GetComponent<VideoPlayer>();
            videoPlayer.url = path;

            DecorationContainer.transform.localScale = new Vector3(1f / 2f, 0.75f / 2f, 1.0f);
        }
    }
}